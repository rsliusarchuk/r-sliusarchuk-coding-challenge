import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToOne,
} from 'typeorm';
import { IssueStatusEnum } from './enums/status.enum';
import { BaseEntity } from '../common/entities/base.entity';
import { User } from '../users/user.entity';

@Entity()
export class Issue extends BaseEntity {
  @Column()
  title: string;

  @Column({ type: 'text' })
  description: string;

  @Column({
    type: 'enum',
    enum: IssueStatusEnum,
    default: IssueStatusEnum.OPEN,
  })
  @Index('status-idx')
  status: IssueStatusEnum;

  @OneToOne(() => User, (user) => user.processingIssue)
  @JoinColumn()
  processingBy: User;

  @ManyToOne(() => User, (user) => user.processedIssues)
  @JoinColumn()
  @Index({ unique: false })
  processedBy: User;
}
