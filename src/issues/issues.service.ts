import { Injectable, Logger } from '@nestjs/common';
import { CreateIssueDto } from './dto/create-issue.dto';
import { IssuesRepository } from './issues.repository';
import { Issue } from './issue.entity';
import { User } from '../users/user.entity';
import { FindOneOptions } from 'typeorm';

@Injectable()
export class IssuesService {
  private readonly logger = new Logger(this.constructor.name);

  constructor(private readonly issueRepository: IssuesRepository) {}

  findAll(): Promise<Issue[]> {
    this.logger.log('Find all issues');
    return this.issueRepository.find();
  }

  findOneById(id: string, options: FindOneOptions): Promise<Issue> {
    this.logger.log(`Get issue by id. Id: ${id}`);
    return this.issueRepository.getIssueById(id, options);
  }

  create(createIssueDto: CreateIssueDto): Promise<Issue> {
    this.logger.log(`Create issue. Data: ${JSON.stringify(createIssueDto)}`);
    return this.issueRepository.createIssue(createIssueDto);
  }

  resolveIssue(issue: Issue): Promise<Issue> {
    this.logger.log(`Resolve issue. Issue: ${JSON.stringify(issue)}`);
    return this.issueRepository.resolveIssue(issue);
  }

  assignAgentToUnassignedIssue(agent: User): Promise<Issue> {
    this.logger.log(
      `Assign unassigned issue to agent. Agent: ${JSON.stringify(agent)}`,
    );
    return this.issueRepository.assignAgentToUnassignedIssue(agent);
  }

  findFreeAgentAndAssignToIssue(issue: Issue): Promise<Issue> {
    this.logger.log(
      `Find free agent and assign to issue. Issue: ${JSON.stringify(issue)}`,
    );
    return this.issueRepository.findFreeAgentAndAssignToIssue(issue);
  }
}
