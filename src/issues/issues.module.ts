import { Module } from '@nestjs/common';
import { IssuesService } from './issues.service';
import { IssuesController } from './issues.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IssuesRepository } from './issues.repository';

@Module({
  imports: [TypeOrmModule.forFeature([IssuesRepository])],
  controllers: [IssuesController],
  providers: [IssuesService],
  exports: [],
})
export class IssuesModule {}
