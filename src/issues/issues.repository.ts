import {
  EntityRepository,
  FindOneOptions,
  getManager,
  Repository,
} from 'typeorm';
import { Issue } from './issue.entity';
import { IssueStatusEnum } from './enums/status.enum';
import { User } from '../users/user.entity';
import { InternalServerErrorException, Logger } from '@nestjs/common';
import { CreateIssueDto } from './dto/create-issue.dto';
import { UserRoleEnum } from '../users/enums/user.role.enum';

@EntityRepository(Issue)
export class IssuesRepository extends Repository<Issue> {
  private readonly logger = new Logger(this.constructor.name);

  async getIssueById(id: string, options: FindOneOptions): Promise<Issue> {
    try {
      this.logger.log(`Get issue by id. Id: ${id}`);
      return await this.findOne(id, options);
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException('Something went wrong');
    }
  }

  async createIssue(createIssueDto: CreateIssueDto): Promise<Issue> {
    try {
      const issue = new Issue();

      issue.title = createIssueDto.title;
      issue.description = createIssueDto.description;

      this.logger.log(`Create issue. Data: ${JSON.stringify(issue)}`);
      return await this.save(issue);
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException('Something went wrong');
    }
  }

  async resolveIssue(issue: Issue): Promise<Issue> {
    try {
      issue.status = IssueStatusEnum.RESOLVED;
      issue.processedBy = issue.processingBy;
      issue.processingBy = null;

      this.logger.log(`Resolve issue. Data: ${JSON.stringify(issue)}`);
      return this.save(issue);
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException('Something went wrong');
    }
  }

  async assignAgentToUnassignedIssue(agent: User): Promise<Issue | undefined> {
    try {
      return await getManager().transaction(
        async (transactionalEntityManager) => {
          this.logger.log('Find unassigned issue');
          const unassignedIssue = await transactionalEntityManager.findOne(
            Issue,
            {
              status: IssueStatusEnum.OPEN,
            },
          );

          if (!unassignedIssue) {
            return;
          }

          unassignedIssue.status = IssueStatusEnum.ASSIGNED;
          unassignedIssue.processingBy = agent;

          this.logger.log(
            `Assign agent to issue. Data: ${JSON.stringify(unassignedIssue)}`,
          );
          return await transactionalEntityManager.save(unassignedIssue);
        },
      );
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException('Something went wrong');
    }
  }

  async findFreeAgentAndAssignToIssue(issue: Issue): Promise<Issue> {
    try {
      return await getManager().transaction(
        async (transactionalEntityManager) => {
          this.logger.log('Prepare get busy agents query');
          const busyAgentsQuery = transactionalEntityManager
            .createQueryBuilder(Issue, 'issue')
            .select('issue.processingBy')
            .where('issue.processingBy IS NOT NULL');

          this.logger.log('Find free agent');
          const freeAgent = await transactionalEntityManager
            .createQueryBuilder(User, 'user')
            .where(`user.id NOT IN (${busyAgentsQuery.getQuery()})`)
            .andWhere('user.role = :role', { role: UserRoleEnum.SUPPORT })
            .getOne();

          if (freeAgent) {
            issue.processingBy = freeAgent;
            issue.status = IssueStatusEnum.ASSIGNED;

            this.logger.log(`Update issue. Data: ${JSON.stringify(issue)}`);
            return await transactionalEntityManager.save(issue);
          }

          return issue;
        },
      );
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException('Something went wrong');
    }
  }
}
