import { User } from '../../users/user.entity';
import { Exclude, Transform } from 'class-transformer';

export class ResponseResolveIssueDto {
  id: string;

  title: string;

  description: string;

  status: string;

  @Transform(({ value }) => value.id)
  processedBy: User;

  @Exclude()
  processingBy: User;

  createdAt: Date;

  updatedAt: Date;
}
