import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Logger,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
} from '@nestjs/common';
import { IssuesService } from './issues.service';
import { CreateIssueDto } from './dto/create-issue.dto';
import { IssueStatusEnum } from './enums/status.enum';
import { Issue } from './issue.entity';
import { plainToClass } from 'class-transformer';
import { ResponseResolveIssueDto } from './dto/response-resolve-issue.dto';

@Controller('issues')
export class IssuesController {
  private readonly logger = new Logger(this.constructor.name);

  constructor(private readonly issuesService: IssuesService) {}

  @Get()
  findAll(): Promise<Issue[]> {
    this.logger.log('Find all issues');
    return this.issuesService.findAll();
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create(@Body() createIssueDto: CreateIssueDto): Promise<Issue> {
    this.logger.log(`Create issue. Data: ${JSON.stringify(createIssueDto)}`);
    const issue = await this.issuesService.create(createIssueDto);

    this.logger.log(
      `Find free agent and assign to issue. Issue: ${JSON.stringify(issue)}`,
    );
    return this.issuesService.findFreeAgentAndAssignToIssue(issue);
  }

  @Patch(':id/resolve')
  async resolveIssue(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<ResponseResolveIssueDto> {
    this.logger.log(`Get issue by id. Id: ${id}`);
    const issue = await this.issuesService.findOneById(id, {
      relations: ['processingBy'],
    });

    if (!issue) {
      throw new NotFoundException('Issue not found');
    }

    if (issue.status !== IssueStatusEnum.ASSIGNED) {
      throw new BadRequestException('Only assigned issue could be resolved');
    }

    const agent = issue.processingBy;

    if (!agent) {
      throw new BadRequestException(
        'Issue can not be resolved without assigned agent',
      );
    }

    this.logger.log(`Resolve issue. Data: ${JSON.stringify(issue)}`);
    const updatedIssue = await this.issuesService.resolveIssue(issue);

    this.logger.log(
      `Assign unassigned issue to agent. Agent: ${JSON.stringify(agent)}`,
    );
    await this.issuesService.assignAgentToUnassignedIssue(agent);

    return plainToClass(ResponseResolveIssueDto, updatedIssue);
  }
}
