import { Column, Entity, Index, OneToMany, OneToOne } from 'typeorm';
import { BaseEntity } from '../common/entities/base.entity';
import { UserRoleEnum } from './enums/user.role.enum';
import { Issue } from '../issues/issue.entity';

@Entity()
export class User extends BaseEntity {
  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({
    type: 'enum',
    enum: UserRoleEnum,
  })
  @Index('user-role-idx')
  role: UserRoleEnum;

  @OneToOne(() => Issue, (issue) => issue.processingBy)
  processingIssue: Issue;

  @OneToMany(() => Issue, (issue) => issue.processedBy)
  processedIssues: Issue[];
}
