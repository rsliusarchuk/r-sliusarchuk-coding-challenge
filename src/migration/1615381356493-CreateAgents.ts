import { MigrationInterface, QueryRunner } from 'typeorm';
import { Logger } from '@nestjs/common';
import { User } from '../users/user.entity';
import { UserRoleEnum } from '../users/enums/user.role.enum';

export class CreateAgents1615381356493 implements MigrationInterface {
  private readonly logger = new Logger(this.constructor.name);

  public async up(queryRunner: QueryRunner): Promise<void> {
    this.logger.log('Create agents');
    await queryRunner.connection.getRepository(User).save([
      {
        firstName: 'John',
        lastName: 'Smith',
        role: UserRoleEnum.SUPPORT,
      },
      {
        firstName: 'Betty',
        lastName: 'Dow',
        role: UserRoleEnum.SUPPORT,
      },
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    return;
  }
}
