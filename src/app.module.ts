import { Module } from '@nestjs/common';
import { IssuesModule } from './issues/issues.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from './config/config.service';

@Module({
  imports: [
    TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
    IssuesModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
