import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { HttpStatus, INestApplication, ValidationPipe } from '@nestjs/common';
import { IssuesModule } from '../src/issues/issues.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from '../src/config/config.service';
import { Repository } from 'typeorm';
import { Issue } from '../src/issues/issue.entity';
import { User } from '../src/users/user.entity';
import { UserRoleEnum } from '../src/users/enums/user.role.enum';
import { IssueStatusEnum } from '../src/issues/enums/status.enum';

describe('Issues', () => {
  let app: INestApplication;
  let issuesRepository: Repository<Issue>;
  let usersRepository: Repository<User>;
  const createdIssues = [];

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        IssuesModule,
        TypeOrmModule.forRoot(configService.getTypeOrmConfigForE2E()),
      ],
    }).compile();

    app = moduleRef.createNestApplication();

    app.useGlobalPipes(
      new ValidationPipe({
        forbidNonWhitelisted: true,
        forbidUnknownValues: true,
      }),
    );

    await app.init();

    issuesRepository = moduleRef.get('IssuesRepository');
    usersRepository = issuesRepository.manager.connection.getRepository(User);
    await issuesRepository.query('DELETE FROM "issue";');
    await usersRepository.query('DELETE FROM "user";');

    await usersRepository.save([
      {
        firstName: 'John',
        lastName: 'Smith',
        role: UserRoleEnum.SUPPORT,
      },
      {
        firstName: 'Betty',
        lastName: 'Dow',
        role: UserRoleEnum.SUPPORT,
      },
    ]);
  });

  it('Check if agents exists', async () => {
    const agents = await usersRepository.find({ role: UserRoleEnum.SUPPORT });
    expect(agents.length).toBe(2);
  });

  describe('/POST issues', () => {
    const issueDataList = [
      {
        title: 'Title1',
        description: 'Description1',
      },
      {
        title: 'Title2',
        description: 'Description2',
      },
      {
        title: 'Title3',
        description: 'Description3',
      },
    ];
    const titleIsMissing = { description: 'test' };
    const descriptionIsMissing = { title: 'test' };

    it('Should receive 201', async () => {
      for (const issueDataListElement of issueDataList) {
        const { body } = await request(app.getHttpServer())
          .post('/issues')
          .send(issueDataListElement)
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(HttpStatus.CREATED);
        createdIssues.push(body);
      }
    });

    it(`Should have status ${IssueStatusEnum.ASSIGNED}`, async () => {
      expect(createdIssues[0]).toHaveProperty(
        'status',
        IssueStatusEnum.ASSIGNED,
      );
      expect(createdIssues[1]).toHaveProperty(
        'status',
        IssueStatusEnum.ASSIGNED,
      );
    });

    it(`Should have status ${IssueStatusEnum.OPEN} due to non free agents`, async () => {
      expect(createdIssues[2]).toHaveProperty('status', IssueStatusEnum.OPEN);
    });

    it('Should fail - title is missing', async () => {
      const { body } = await request(app.getHttpServer())
        .post('/issues')
        .send(titleIsMissing)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(HttpStatus.BAD_REQUEST);

      expect(body).toHaveProperty('message', ['title should not be empty']);
    });

    it('Should fail - description is missing', async () => {
      const { body } = await request(app.getHttpServer())
        .post('/issues')
        .send(descriptionIsMissing)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(HttpStatus.BAD_REQUEST);

      expect(body).toHaveProperty('message', [
        'description should not be empty',
      ]);
    });
  });

  describe('/PATCH issues/:id/resolve', () => {
    it('Should fail with 404', async () => {
      await request(app.getHttpServer())
        .patch('/issues/24490471-8480-41a5-a6dc-5f70b4f58346/resolve')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(HttpStatus.NOT_FOUND);
    });

    it('Should fail - can not resolve not assigned issue', async () => {
      const { body } = await request(app.getHttpServer())
        .patch(`/issues/${createdIssues[2].id}/resolve`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(HttpStatus.BAD_REQUEST);

      expect(body).toHaveProperty(
        'message',
        'Only assigned issue could be resolved',
      );
    });

    it(`Should update status to ${IssueStatusEnum.RESOLVED}`, async () => {
      const { body } = await request(app.getHttpServer())
        .patch(`/issues/${createdIssues[0].id}/resolve`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(HttpStatus.OK);
      expect(body).toHaveProperty('status', IssueStatusEnum.RESOLVED);
    });

    it(`Should update status to ${IssueStatusEnum.ASSIGNED}`, async () => {
      const issue = await issuesRepository.findOne(createdIssues[2].id);
      expect(issue).toHaveProperty('status', IssueStatusEnum.ASSIGNED);
    });

    it('Should shift processingBy to processedBy', async () => {
      const issue = await issuesRepository.findOne(createdIssues[0].id, {
        relations: ['processingBy', 'processedBy'],
      });
      expect(issue.processingBy).toBeFalsy();
      expect(issue.processedBy.id).toBeTruthy();
    });
  });

  it('/GET issues', async () => {
    await request(app.getHttpServer()).get('/issues').expect(HttpStatus.OK);
  });

  afterAll(async () => {
    await issuesRepository.query('DELETE FROM "issue";');
    await usersRepository.query('DELETE FROM "user";');
    await app.close();
  });
});
